extends Area2D

var coin_bit_scene = preload("res://Scenes/CoinBit.tscn")

func _ready():
	GameState.nodes["coin_icon"] = self
	pass
	
func launch_coin_burst(amount, at_position):
	call_deferred("_deferred_launch_coin_burst", amount, at_position)
	pass
	
func _deferred_launch_coin_burst(amount:int, at_position:Vector2) -> void:
	for _i in range(amount):
		var cb = coin_bit_scene.instance()
		
		add_child(cb)
		cb.global_position = at_position
		cb.launch()
		
		pass
	pass

func _on_Coin_area_entered(area):
	# Debug.print("Coin entered area!")
	
	if area.is_in_group("CoinBit"):
		GameState.add_coins(1)
		area.queue_free()
		$Tween.interpolate_property($Sprite, "scale", Vector2(1.22, 1.22), Vector2(1.0, 1.0), 0.45, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
		$Tween.start()
		pass
	
	pass
