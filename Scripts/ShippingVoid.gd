extends Area2D

onready var coin_node = $"/root/GameMode/Camera/Camera2D/Coin"

func _ready():
	pass


func _on_ShippingVoid_body_entered(body):
	if body.is_in_group("Shippable"):
		coin_node.launch_coin_burst(body.value, body.global_position)
		pass
	
	if body is RigidBody2D:
		body.queue_free()
	pass



func _on_SplashArea_body_entered(body):
	#if body.is_in_group("Shippable"):
	$VisibilityNotifier2D.global_position = body.global_position
	if not $VisibilityNotifier2D.is_on_screen():
		Debug.tpr("shipping off screen, no particles")
		return
	var particles = preload("res://Scenes/SplashParticles.tscn").instance()
	add_child(particles)
	particles.global_position = body.global_position
	particles.emitting = true
	pass
