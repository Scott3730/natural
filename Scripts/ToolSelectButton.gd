tool
extends Area2D

export(String, "water", "poop") var tool_type = "water" setget _set_tool_type
var textures = {
	"water":preload("res://Sprites/UI/water_button.png"),
	"poop":preload("res://Sprites/UI/poop_button.png")
}

var scales = {
	"base":Vector2(1.0, 1.0),
	"hover":Vector2(1.1, 1.1),
	"active":Vector2(1.22, 1.22)
	}
	
var modes = {
	"base":true,
	"hover":false,
	"active":false
	}

func _ready():
	$Sprite.texture = textures[tool_type]
	pass
	
func _set_tool_type(val:String):
	tool_type = val
	if Engine.is_editor_hint():
		$Sprite.texture = textures[tool_type]
	else:
		Debug.epr("Button tool type should never change unless in tool mode!")
		print(get_stack())
	pass

func _on_ToolIcon_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event is InputEventMouseButton and not event.is_pressed():
		if not modes.active:
			$"/root/GameMode".set_tool_type(tool_type)
			$"..".set_currently_active_button(self)
		else:
			$"/root/GameMode".set_tool_type("none")
			$"..".set_currently_active_button(null)
		pass
	pass
	
func update_scale() -> void:
	$Tween.stop_all()
	
	if modes.active:
		$Tween.interpolate_property($Sprite, "scale", null, scales.active, 0.15, Tween.TRANS_SINE, Tween.EASE_OUT)
	elif modes.hover:
		$Tween.interpolate_property($Sprite, "scale", null, scales.hover, 0.1, Tween.TRANS_SINE, Tween.EASE_OUT)
	elif modes.base:
		$Tween.interpolate_property($Sprite, "scale", null, scales.base, 0.23, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	else:
		Debug.err("ToolButton state mismanaged, no modes enabled: " + str(modes))
		
	$Tween.start()
	pass

func activate() -> void:
	modes.active = true
	update_scale()
	pass

func deactivate() -> void:
	modes.active = false
	update_scale()
	pass

func _on_ToolIcon_mouse_entered() -> void:
	modes.hover = true
	update_scale()
	pass

func _on_ToolIcon_mouse_exited() -> void:
	modes.hover = false
	update_scale()
	pass
