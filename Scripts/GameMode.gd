extends Node2D

var current_tool : String = "none"
var tool_state : String = "inactive"

var spawns_per_second : float = 35.0
var spawns_per_round : float = 3.0
var tool_body_timer : float = 1.0 / (spawns_per_second / spawns_per_round)

var scn_particle_body : PackedScene = preload("res://Scenes/ParticleBody.tscn")

var pb_container : Node
var held_item : Node2D = null

func _ready():
	pb_container = Node2D.new()
	pb_container.name = "ToolParticleBodies"
	add_child(pb_container)
	GameState.nodes["game_mode"] = self
	pass
	
func _process(delta:float) -> void:
	tool_body_timer -= delta
	
	if held_item != null and current_tool != "none":
		deactivate_tool()
		return
	
	match tool_state:
		"active":
			if tool_body_timer < 0.0:
				tool_body_timer = 1.0 / (spawns_per_second / spawns_per_round)
				for i in spawns_per_round:
					var offset_amount = 15.0
					var r_offset = get_global_mouse_position() + Vector2(rand_range(-offset_amount, offset_amount), rand_range(-offset_amount, offset_amount))
					var r_impulse = Vector2(rand_range(-450.0, -120.0), 420.0)
					spawn_tool_particle(current_tool, r_offset, r_impulse)
				
			pass
	pass

func _on_Area2D_input_event(_viewport, event, _shape_idx):
	if event is InputEventMouseButton: 
		if event.pressed and GameState.handle_click():
			activate_tool()
		elif not event.pressed:
			tool_state = "none"
	pass
	
func spawn_tool_particle(tool_type, offset, impulse):
	var tb : RigidBody2D = scn_particle_body.instance() as RigidBody2D
	tb.set_tool_type(tool_type)
	pb_container.add_child(tb)
	tb.transform.origin = offset
	tb.apply_impulse(Vector2.ZERO, impulse)
	pass

func activate_tool() -> void:
	if current_tool != "none":
		tool_state = "active"
	pass
	
func deactivate_tool() -> void:
	set_tool_type("none")
	GameState.nodes.camera.get_node("Camera2D/ToolButtons").deactivate_current_button()
	pass

func _on_ToolArea_mouse_exited() -> void:
	tool_state = "none"
	pass


func _on_ToolArea_mouse_entered() -> void:
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and GameState.handle_click():
		activate_tool()
		pass
	pass
	
func set_tool_type(type:String) -> void:
	var tool_particle_textures = {"poop":preload("res://Sprites/Props/poop_particles.png"), "water":preload("res://Sprites/Props/water_particles.png")}
	Debug.print("New tool type: " + type)
	if type == "none":
		GameState.nodes.mouse_point.sprite.visible = false
	else:
		GameState.nodes.mouse_point.sprite.texture = tool_particle_textures[type]
		GameState.nodes.mouse_point.sprite.visible = true
		
	current_tool = type
	pass
