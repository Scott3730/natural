extends Node2D

var revs_per_second = 30.0
var _elapsed = 0.0

var tool_type = "water"

var spout_body_timer = 0.0
var spout_spawns_per_second = 15.0


func _ready():
	pass
	
func _process(delta: float) -> void:
	_elapsed += delta;
	_spin(delta)
	_fire_particles(delta)
	pass
	
func _spin(delta:float) -> void:
	#$spout.scale.x = sin(_elapsed * revs_per_second)
	pass

func _fire_particles(delta:float) -> void:
	spout_body_timer -= delta
	
	
	if spout_body_timer < 0.0:
		spout_body_timer = 1.0 / (spout_spawns_per_second)
		
		var spout_drip = randi() % 2 == 1
		var spout_right = randi() % 2 == 1
		
		if spout_drip:
			var dr_impulse = Vector2(rand_range(200.0 , -200.0), 120.0)
			GameState.nodes.game_mode.spawn_tool_particle(tool_type, global_position + ($SpoutCentre.position), dr_impulse)
		
		var r_impulse = Vector2(rand_range(650.0 * (1.0 if spout_right else -1.0), 220.0 * (1.0 if spout_right else -1.0)), 120.0)
		GameState.nodes.game_mode.spawn_tool_particle(tool_type, global_position + ($SpoutRight.position if spout_right else $SpoutLeft.position), r_impulse)
	pass