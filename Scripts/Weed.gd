extends Sprite

var elapsed_time = 0.0
var scale_factor = 0.051

func _ready():
	pass

func _process(delta):
	elapsed_time += delta * 1.24
	scale.y = 1.0 + (sin(elapsed_time) * scale_factor)
	scale.x = 1.0 - (sin(elapsed_time) * scale_factor * 0.65)
	pass
