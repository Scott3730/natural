extends Node

var tags = {
	"error":true, 
	"notag":true, 
	"temp":true, 
	"info":true, 
	"debug":true, 
	"phys":true,
	"verbose":false,
	"taghunt":false
	}

var visible_tag_length = 4

func _ready():
	for a in tags.keys():
		if a.length() > visible_tag_length and tags[a]:
			visible_tag_length = a.length()
	pass

func err(output) -> void:
	epr(output)

func tpr(output) -> void: # temp_print
	gpr("temp", output)

func ipr(output) -> void:
	gpr("info", output)

func dpr(output) -> void:
	gpr("debug", output)

func print(output) -> void:
	gpr("notag", output)

func epr(output) -> void:
	gpr("error", output)

func gpr(tag:String, output) -> void: # general_print
	if tags.has(tag) and tags[tag]:
		var stack = get_stack()
		var stack_entry
		
		for s in stack:
			if s.source != get_script().resource_path:
				stack_entry = s
				break
		
		var split_array = stack_entry.source.split("/")
		var s = split_array[split_array.size() - 1]
		
		var ctag = tag
		
		if ctag.length() < visible_tag_length:
			while ctag.length() < visible_tag_length:
				ctag = ctag.insert(0, " ")
		
		if ctag.length() > visible_tag_length:
			ctag = ctag.substr(0, visible_tag_length)
		
		print("[", ctag, "][", s, ":", stack_entry.line ,"] -> ", output)
		