extends Area2D

var direction = 0.0
#warning-ignore:unused_class_variable
var speed = 1650.0

var spr_start_x = 0.0
var elapsed_time = 0.0

var is_enabled = true
#warning-ignore:unused_class_variable
export(String, "Left", "Right") var move_direction = "Left"

func _ready() -> void:
	spr_start_x = $Sprite.position.x
	pass

func _physics_process(delta: float) -> void:
	if $Sprite.visible:
		elapsed_time += delta * 4.5
		$Sprite.position.x = spr_start_x + (sin(elapsed_time) * 9.0)
		pass
		
	if not is_enabled:
		$"../..".direction = 0.0
	
	pass
	
func disable() -> void:
	self.visible = false
	self.is_enabled = false
	pass

func enable() -> void:
	self.visible = true
	self.is_enabled = true
	pass


func _on_Area2D_mouse_entered() -> void:
	if not is_enabled:
		return
		
	$"../..".direction = -1.0 if move_direction == "Left" else 1.0
	$Sprite.modulate.a = 0.0
	$Sprite.visible = true
	
	$Tween.stop_all()
	$Tween.remove_all()
	$Tween.interpolate_property($Sprite, "modulate:a", null, 1.0, 0.35, Tween.TRANS_CUBIC, Tween.EASE_IN, 0.0)
	$Tween.start()
	pass


func _on_Area2D_mouse_exited() -> void:
	if not is_enabled:
		return
		
	$"../..".direction = 0.0
	$Sprite.position.x = spr_start_x
	
	$Tween.stop_all()
	$Tween.remove_all()
	$Tween.interpolate_property($Sprite, "modulate:a", null, 0.0, 0.25, Tween.TRANS_CUBIC, Tween.EASE_IN, 0.0)
	$Tween.interpolate_property($Sprite, "visible", null, false, 0.01, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 0.25)
	$Tween.start()
	pass
