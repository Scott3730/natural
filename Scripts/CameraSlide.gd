extends Node2D

var nx_limit = -8460
var px_limit = 7480

var terminal_velocity = 6500.0
var velocity = 0.0
var accel = 1600.0
var direction = 0.0
var drag = 0.91
var dead_zone = 0.2

var ground_camera_y:float

var tween_state = "stopped"

var shake_timer:float = 0.0
var shake_amount:int = 0

func _ready():
	GameState.nodes["camera"] = self
	ground_camera_y = position.y
	
	$Tween.connect("tween_completed", self, "_enable_scroll_areas")
	pass
	
func _process(delta: float) -> void:
	shake_timer -= delta
	if shake_timer > 0.0:
		$Camera2D.offset = Vector2(rand_range(-shake_amount, shake_amount), rand_range(-shake_amount, shake_amount))
	else:
		$Camera2D.offset = Vector2.ZERO
	pass

func _physics_process(delta):
	if direction == 0.0:
		velocity *= drag

	#if direction != 0.0:
	velocity = clamp(velocity + (accel * direction * delta), -terminal_velocity, terminal_velocity)
	position.x = clamp(position.x + (velocity * delta), nx_limit, px_limit)

	if sign(direction) == -sign(velocity):
		velocity /= 4.5

	if abs(velocity) < dead_zone:
		velocity = 0.0
		
	pass
	
func _enable_scroll_areas(object, key) -> void:
	if tween_state == "going_down":
		$Camera2D/LeftScrollArea.enable()
		$Camera2D/RightScrollArea.enable()
		$Camera2D/ToolButtons.enable()
		tween_state = "stopped"
	pass
	
func transition_camera_up() -> void:
	$Tween.interpolate_property(self, "position", null, Vector2(5700, ground_camera_y - 1700), 1.35, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	GameState.nodes.clouds.part_clouds()
	tween_state = "going_up"
	$Tween.start()
	$Camera2D/LeftScrollArea.disable()
	$Camera2D/RightScrollArea.disable()
	$Camera2D/ToolButtons.disable()
	pass
	
func transition_camera_down() -> void:
	$Tween.interpolate_property(self, "position", null, Vector2(5700, ground_camera_y), 1.35, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	GameState.nodes.clouds.close_clouds()
	tween_state = "going_down"
	$Tween.start()
	pass
	
func shake(duration:float, amount:int = 3) -> void:
	shake_timer = duration
	shake_amount = amount
	pass