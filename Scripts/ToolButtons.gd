extends Node2D

const ToolSelectButtonScript = preload("res://Scripts/ToolSelectButton.gd")
var currently_active_button:ToolSelectButtonScript = null
var is_enabled = true

func deactivate_current_button():
	if currently_active_button != null:
		currently_active_button.deactivate()

func set_currently_active_button(button:ToolSelectButtonScript):
	if not is_enabled:
		return
	if currently_active_button != null:
		currently_active_button.deactivate()
		
	currently_active_button = button
	
	if currently_active_button != null:
		currently_active_button.activate()
	
	pass

func enable() -> void:
	self.visible = true
	self.is_enabled = true
	pass
	
func disable() -> void:
	self.visible = false
	self.is_enabled = false
	pass