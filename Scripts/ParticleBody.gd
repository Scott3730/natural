extends RigidBody2D

var tool_type = "none"
var ttl = 2.5
var tltl = ttl # time left to live

var textures = {
	"water":preload("res://Sprites/Props/water_particles.png"),
	"poop":preload("res://Sprites/Props/poop_particles.png")
}

func _ready():
	
	pass
	
func _physics_process(delta: float) -> void:
	tltl -= delta
	
	if tltl < 0.0:
		queue_free()
	pass

func set_tool_type(type:String) -> void:
	add_to_group("tool_particle")
	tool_type = type
	$Sprite.texture = textures[tool_type]
	pass
