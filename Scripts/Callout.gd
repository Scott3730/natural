extends Sprite

var elapsed_time = 0.0
var bob_height = 7.0
var bob_speed = 5.5
var initial_y = 0.0

var textures = {
	"water":preload("res://Sprites/UI/water_callout.png"), 
	"poop":preload("res://Sprites/UI/poop_callout.png"), 
	"no_space":preload("res://Sprites/UI/no_space_callout.png"),
	"water_progress":preload("res://Sprites/UI/water_pb.png"), 
	"poop_progress":preload("res://Sprites/UI/poop_pb.png")
	}
var active_callouts = {"water":false, "poop":false, "no_space":false}

var callout_override = {"type": "none", "timer": 0.0}

onready var plant = $".."

func _ready() -> void:
	
	plant.connect("water_filled", self, "_on_plant_water_filled")
	plant.connect("poop_filled", self, "_on_plant_poop_filled")
	
	reset()
	_select_callout()
	pass

func _process(delta:float) -> void:
	elapsed_time += delta * bob_speed
	position.y = initial_y + sin(elapsed_time) * bob_height
	rotation_degrees = 0.0
	
	callout_override.timer -= delta
	if callout_override.timer < 0.0:
		callout_override.type = "none"
		z_index = 0
		
	if plant.state.growth == "Growing":
		if plant.timers.water_stage <= 0.0 and plant.state.water_stages > 0 and plant.state.water < plant.state.water_max:
			activate_callout_type("water")
		elif plant.timers.poop_stage <= 0.0 and plant.state.poop_stages > 0 and plant.state.poop < plant.state.poop_max:
			activate_callout_type("poop")
	
	pass

func activate_callout_type(type:String) -> void:
	self.visible = true
	active_callouts[type] = true
	get_parent().add_to_group("needs_" + type)
	_select_callout()
	pass
	
func deactivate_callout_type(type:String) -> void:
	active_callouts[type] = false
	if get_parent().is_in_group("needs_" + type):
		Debug.ipr("Removing from group needs_" + type)
		get_parent().remove_from_group("needs_" + type)
	_select_callout()
	pass

func _select_callout() -> void:
	$TextureProgress.visible = false
	
	if callout_override.timer > 0.0 and active_callouts[callout_override.type]:
		_setup_callout(callout_override.type)
	elif active_callouts.no_space:
		_setup_callout("no_space")
	elif active_callouts.water:
		_setup_callout("water")
	elif active_callouts.poop:
		_setup_callout("poop")
	else:
		self.visible = false
	
func _setup_callout(type:String) -> void:
	self.texture = textures[type]
	if textures.has(type+"_progress") and plant.state[type] > 0:
		$TextureProgress.texture_progress = textures[type+"_progress"]
		$TextureProgress.value = plant.state[type]
		$TextureProgress.max_value = plant.state[type+"_max"]
		$TextureProgress.visible = true
	pass

func reset():
	initial_y = position.y
	pass

func override_callout_priority(callout_type:String, override_time:float = 5.0):
	callout_override.type = callout_type
	callout_override.timer = override_time
	z_index = 15
	activate_callout_type(callout_type)
	#Debug.tpr("Overriding callout!" + str(callout_override) + str(active_callouts))
	pass
	
func _on_plant_water_filled() -> void:
	deactivate_callout_type("water")
	pass

func _on_plant_poop_filled() -> void:
	deactivate_callout_type("poop")
	pass